package com.tere.firebaseintro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var TAG = "FirebaseDebug"
        val db = FirebaseFirestore.getInstance()

        /*METODO PARA TRAER INFO EN TIEMPO REAL PARA TOOODA LA COLECCIÓN*/
        db.collection("books")
            .addSnapshotListener { value, error ->
                value.let {
                    for (document in value?.documents!!){
                        Log.d(TAG,"Name ${document?.getString("name")}")
                        Log.d(TAG,"Description ${document?.getString("description")}")
                    }
                }
            }
        /* METODO PARA TRAER INFO EN TIEMPO REAL
        db.collection("books").document("001")
            .addSnapshotListener { value, error ->
            value.let {
                document ->
                document.let {
                    Log.d(TAG,"Name ${document?.getString("name")}")
                    Log.d(TAG,"Description ${document?.getString("description")}")
                }
            }
        }
        */
        /* GUARDAR REGISTRO
        db.collection("books").document().set(Book(name = "Los juegos del hambre", description = "Accion"))
            .addOnSuccessListener {
                Log.d(TAG,"Book Saved")
            }.addOnFailureListener {exeption ->
                Log.d(TAG,"Something wrong")
            }
        */
        

        /* MÉTODO PARA BUSCAR POR NOMBRE
        db.collection("books").whereEqualTo("name","Harry Potter").get()
            .addOnSuccessListener {
                    result ->
                for(document in result){
                    Log.d(TAG,"Name ${document.getString("name")}")
                    Log.d(TAG, "Description ${document.getString("description")}")

                }
        */

        /* METODO PARA TRAER CON UN ID EN ESPECÍFICO
        db.collection("books").document("001").get()
            .addOnSuccessListener { document ->
                document.let {
                    Log.d(TAG,"Name ${document.getString("name")}")
                    Log.d(TAG,"Description ${document.getString("description")}")
                }
            }
        */

        /* METODO PARA TRAER TODOS LOS DOCUMENTOS
        Log.d(TAG,"Get all documents")

        db.collection("books").get().addOnSuccessListener {
            result ->
            for(document in result){
                Log.d(TAG,"Name ${document.getString("name")}")
                Log.d(TAG, "Desciption ${document.getString("description")}")

            }
        }
         */

    }
    data class Book(var name:String = "",var description:String="")
}